package com.dev.rick3rri.project;

//Додаємо бібліотеки
import android.app.Activity;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//Створюємо основний клас
public class MainActivity extends Activity {
//Ініціалізюємо кнопки та камеру.
    Button button;
    Button button2;
    Camera cam = Camera.open();
    Camera.Parameters p = cam.getParameters();

    @Override
    protected void onCreate(Bundle savedInstabceState) {
        super.onCreate(savedInstabceState);
        setContentView(R.layout.activity_main);

        button = (Button)findViewById(R.id.button); //опис кнопки 1
        button2 = (Button)findViewById(R.id.button2); //опис кнопки 2

        // Відстеження натискання кнопки вмикання
        View.OnClickListener KNOPKAONN = new View.OnClickListener(){
            @Override
        public void onClick(View v) {
                MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.beep);
                mediaPlayer.start();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                cam.setParameters(p);
                cam.startPreview();
            }
        };

        //Відстеження натискання кнопки вимикання
    View.OnClickListener KNOPKAOFF = new View.OnClickListener() {
    @Override
        //ініціалізація медіа плеєра й вмикання файлу beep.mp3
    public void onClick(View v) {
        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.beep);
        mediaPlayer.start();
        p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF); //Вимикаємо фонарик
        cam.setParameters(p);
        cam.startPreview();
    }
};
        //Відстеження, яка ннопка натиснута
    button.setOnClickListener(KNOPKAONN);
    button2.setOnClickListener(KNOPKAOFF);
    }
}